var express = require('express');
var router = express.Router();
var sql = require('mssql');

const config = {
  user: 'sa',
  password: '123456aA@',
  server: '127.0.0.1', // or IP address
  database: 'Luckydraw',
  trustServerCertificate: true,
  options: {
    port: 1433,
    encrypt: true, // Use this if you're on Windows Azure
  },
};

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});
//get all customers

router.post('/customers', async (req, res, next) => {
  try {
    const pool = await sql.connect(config)
      .then(() => {
        console.log('Connected to SQL Server');
      })
      .catch((err) => {
        console.error('Error connecting to SQL Server:', err);
      })

    const transaction = new sql.Transaction(pool);
    await transaction.begin();
    const request = new sql.Request();
    const checkExists = await request.query(`SELECT * FROM dbo.lucky_game where status_game=''`);
    await transaction.commit();
    res.json({ customers: checkExists.recordset });
  } catch (error) {
    // Handle errors
    console.error('Error making API call:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
})

router.post('/update_status', async (req, res, next) => {
  try {
    const pool = await sql.connect(config)
      .then(() => {
        console.log('Connected to SQL Server');
      })
      .catch((err) => {
        console.error('Error connecting to SQL Server:', err);
      })
    const manv = req.body.manv;

    const transaction = new sql.Transaction(pool);
    await transaction.begin();
    const request = new sql.Request();
    const checkExists = await request.query(`SELECT * FROM dbo.lucky_game where manv='${manv}'`);
    if (checkExists.recordset.length > 0) {
      await request.query(`Update dbo.lucky_game set status_game=2 where manv='${manv}'`);
    }
    await transaction.commit();
    res.status(200).json({ success: 'success', message: 'Update success' });
  } catch (error) {
    // Handle errors
    console.error('Error making API call:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
})
router.post('/update_status_delete', async (req, res, next) => {
  try {
    const pool = await sql.connect(config)
      .then(() => {
        console.log('Connected to SQL Server');
      })
      .catch((err) => {
        console.error('Error connecting to SQL Server:', err);
      })
    const manv = req.body.manv;

    const transaction = new sql.Transaction(pool);
    await transaction.begin();
    const request = new sql.Request();
    const checkExists = await request.query(`SELECT * FROM dbo.lucky_game where manv='${manv}'`);
    if (checkExists.recordset.length > 0) {
      await request.query(`Update dbo.lucky_game set status_game=1 where manv='${manv}'`);
    }
    await transaction.commit();
    res.status(200).json({ success: 'success', message: 'Update success' });
  } catch (error) {
    // Handle errors
    console.error('Error making API call:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
})

router.post('/update_status_all', async (req, res, next) => {
  try {
    const pool = await sql.connect(config)
      .then(() => {
        console.log('Connected to SQL Server');
      })
      .catch((err) => {
        console.error('Error connecting to SQL Server:', err);
      })
    const listId = req.body.listmanv;

    const transaction = new sql.Transaction(pool);
    await transaction.begin();
    const request = new sql.Request();
    if (listId.length > 0) {
      listId.forEach(async (item, index) => {
        await request.query(`Update dbo.lucky_game set status_game=1 where manv='${item}'`);
      })
    }
    // const checkExists = await request.query(`SELECT * FROM dbo.lucky_game where manv='${manv}'`);
    // if (checkExists.recordset.length > 0) {
    //   await request.query(`Update dbo.lucky_game set status_game=1 where manv='${manv}'`);
    // }
    await transaction.commit();
    res.status(200).json({ success: 'success', message: 'Update status all success' });
  } catch (error) {
    // Handle errors
    console.error('Error making API call:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
})
router.post('/update_status_received', async (req, res, next) => {
  try {
    const pool = await sql.connect(config)
      .then(() => {
        console.log('Connected to SQL Server');
      })
      .catch((err) => {
        console.error('Error connecting to SQL Server:', err);
      })
    const listId = req.body.listmanv;

    const transaction = new sql.Transaction(pool);
    await transaction.begin();
    const request = new sql.Request();
    if (listId.length > 0) {
      listId.forEach(async (item, index) => {
        await request.query(`Update dbo.lucky_game set status_game=3 where manv='${item}'`);
      })
    }
    // const checkExists = await request.query(`SELECT * FROM dbo.lucky_game where manv='${manv}'`);
    // if (checkExists.recordset.length > 0) {
    //   await request.query(`Update dbo.lucky_game set status_game=1 where manv='${manv}'`);
    // }
    await transaction.commit();
    res.status(200).json({ success: 'success', message: 'Update status all success' });
  } catch (error) {
    // Handle errors
    console.error('Error making API call:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
})

router.post('/customers_winner', async (req, res, next) => {
  try {
    const pool = await sql.connect(config)
      .then(() => {
        console.log('Connected to SQL Server');
      })
      .catch((err) => {
        console.error('Error connecting to SQL Server:', err);
      })

    const transaction = new sql.Transaction(pool);
    await transaction.begin();
    const request = new sql.Request();
    const checkExists = await request.query(`SELECT * FROM dbo.lucky_game where status_game=2`);
    await transaction.commit();
    res.json({ customers: checkExists.recordset });
  } catch (error) {
    // Handle errors
    console.error('Error making API call:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
})
module.exports = router;
